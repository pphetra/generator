from model import Form
from model import Question
from model import MultipleQuestion
from model import Page
from model import Transition
import json

__author__ = 'pphetra'


def dump_questions(form, root):
    root['questions'] = []
    for q in form.get_questions():
        assert isinstance(q, Question)
        obj = {
            'id': q.get_id(),
            'name': q.get_name(),
            'title': q.get_title(),
            'type': q.get_type(),
        }
        if q.is_required():
            obj['validations'] = [
                {
                    'type': 'require',
                    'message': q.get_required_msg()
                }
            ]
        if isinstance(q, MultipleQuestion):
            assert isinstance(q, MultipleQuestion)
            obj['items'] = []
            for answer in q.get_answers():
                obj['items'].append({
                    'id': answer,
                    'text': answer
                })

            if q.get_other():
                obj['freeTextChoiceEnable'] = True
                obj['freeTextId'] = q.get_other()
                obj['freeTextText'] = q.get_other()
                obj['freeTextName'] = q.get_name() + 'Other'

        root['questions'].append(obj)


def dump_pages(form, root):
    root['pages'] = []
    for p in form.get_pages():
        assert isinstance(p, Page)
        obj = {
            'id': p.get_id(),
            'questions': [q.get_id() for q in p.get_questions()]
        }
        root['pages'].append(obj)


def dump_transitions(form, root):
    root['transitions'] = []
    for t in form.get_transitions():
        assert isinstance(t, Transition)
        obj = {
            "from": t.get_from_page_id(),
            "to": t.get_to_page_id(),
            "expression": t.get_expression(),
            "order": t.get_order()
        }
        root['transitions'].append(obj)
    root['transitions'] = sorted(root['transitions'], key=lambda k: "%06d%02d" % (k['from'], k['order']))



def dump(form):
    assert isinstance(form, Form)
    root = {
        "startPageId": form.get_start_page_id()
    }
    dump_questions(form, root)
    dump_pages(form, root)
    dump_transitions(form, root)
    return json.dumps(root, sort_keys=True, indent=4, separators=(',', ': '))
