import re
from StringIO import StringIO
import binascii

from shapely.geometry import box
from pyth.plugins.rtf15.reader import Rtf15Reader
from pyth.plugins.plaintext.writer import PlaintextWriter

from model import *


def collect(fn, collection):
    return filter(lambda x: x, map(fn, collection))


def get_user_info_type(graphic):
    if 'UserInfo' in graphic and 'type' in graphic['UserInfo']:
        return graphic['UserInfo']['type']
    return None


def extract_user_info_graphics(root):
    return filter(get_user_info_type, root['GraphicsList'])


def find_first_by_user_info_type(type_name, collection, fn=None):
    for item in collection:
        if 'UserInfo' in item and item['UserInfo']['type'] == type_name:
            if fn:
                return fn(item)
            else:
                return item
    return None


def extract_text(item):
    return item['Text']['Text']


def parse_rtf(text):
    f = StringIO(text)
    doc = Rtf15Reader.read(f)
    return PlaintextWriter.write(doc).read()


def parse_bounds(bounds):
    ary = (map(float, bounds.replace('{', '').replace('}', '').split(',')))
    x1 = ary[0]
    y1 = ary[1]
    x2 = ary[0] + ary[2]
    y2 = ary[1] + ary[3]
    return box(x1, y1, x2, y2)


def parse_required_message(root):
    return find_first_by_user_info_type('requireText', root['Graphics'], lambda x: parse_rtf(extract_text(x)))


def parse_page(item):
    if 'UserInfo' in item and item['UserInfo']['type'] == 'page':
        return Page(item['ID'], parse_bounds(item['Bounds']))
    return None


def parse_pages(root):
    return collect(parse_page, root['GraphicsList'])


def parse_base_question(root):
    title = None
    name = None
    bound = None
    required = False
    required_msg = None
    for g in root['Graphics']:
        if 'TextPlacement' in g:
            name = parse_rtf(extract_text(g))
            bound = parse_bounds(g['Bounds'])
        if 'UserInfo' in g:
            if g['UserInfo']['type'] == 'title':
                title = parse_rtf(extract_text(g))
            elif g['UserInfo']['type'] == 'requireValidation':
                required = True
                required_msg = parse_required_message(g)

    return {
        'bound': bound,
        'name': name,
        'title': title,
        'required': required,
        'require_msg': required_msg
    }


def parse_answer(ary):
    answers = []
    for item in ary:
        if item['Class'] == 'TableGroup':
            for g in item['Graphics']:
                if 'UserInfo' in g and g['UserInfo']['type'] == 'answer':
                    answers.append(parse_rtf(extract_text(g)))

    answers.reverse()
    return answers


def parse_other_answer(ary):
    return find_first_by_user_info_type('other', ary, lambda x: parse_rtf(extract_text(x)))


def parse_question(root):
    match = re.search('^(text|integer)(Question)$', root['UserInfo']['type'])
    if match and match.group(2) == 'Question':
        kargs = parse_base_question(root)
        kargs['answer_type'] = match.group(1)
        return Question(root['ID'], **kargs)
    else:
        return None


def parse_multiple_question(root):
    match = re.search('^(multiple)(Question)$', root['UserInfo']['type'])
    if match and match.group(2) == 'Question':
        kargs = parse_base_question(root)
        kargs['answer_type'] = root['UserInfo']['selection']
        kargs['answers'] = parse_answer(root['Graphics'])
        kargs['other'] = parse_other_answer(root['Graphics'])
        return MultipleQuestion(root['ID'], **kargs)
    else:
        return None


question_type_map_func = {
    'multipleQuestion': parse_multiple_question,
    'textQuestion': parse_question,
    'integerQuestion': parse_question,
}


def parse_questions(root):
    questions = []
    for g in root['GraphicsList']:
        if 'UserInfo' in g and 'type' in g['UserInfo']:
            model_type = g['UserInfo']['type']
            if model_type in question_type_map_func:
                fn = question_type_map_func[model_type]
                question = fn(g)
                if question:
                    questions.append(question)

    return questions


def parse_transitions(root):
    transitions = []
    tran_map = {}
    for g in root['GraphicsList']:
        if 'UserInfo' in g and g['UserInfo']['type'] == 'transition':
            from_id = g['Tail']['ID']
            to_id = g['Head']['ID']
            order = 0
            if 'order' in g['UserInfo']:
                order = int(g['UserInfo']['order'])
            t = Transition(g['ID'], from_id, to_id, order=order)
            transitions.append(t)
            tran_map[g['ID']] = t

    for g in root['GraphicsList']:
        if 'Line' in g:
            exp = parse_rtf(g['Text']['Text'])
            t_id = g['Line']['ID']
            if t_id in tran_map:
                tran_map[t_id].set_expression(exp)
    return transitions


def parse(root):
    pages = parse_pages(root)
    questions = parse_questions(root)
    transitions = parse_transitions(root)

    return Form(pages, questions, transitions)
